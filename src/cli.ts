#!/usr/bin/env node

import { program } from 'commander';
import { execSync } from 'child_process';

program
  .command('new <workspaceName>')
  .description('Create a new Angular workspace with a standalone library')
  .action((workspaceName) => {
    // 創建一個新的 Angular 工作區
    execSync(`ng new ${workspaceName} --standalone --no-create-application --new-project-root '' `, { stdio: 'inherit' });

    // 導航到工作區目錄
    process.chdir(workspaceName);

    // 創建一個 library
    execSync(`ng generate library ${workspaceName} --standalone`, { stdio: 'inherit' });

});

program
  .command('l <library>')
  .description('Create a Library')
  .action((libraryName) => {
    // 創建一個 Library 工作區
    execSync(`ng g library ${libraryName} --standalone `, { stdio: 'inherit' });

});

program
  .command('a <application>')
  .description('Create a Application')
  .action((appName) => {
    // 創建一個 Application 工作區
    execSync(`ng g application ${appName} --standalone `, { stdio: 'inherit' });

});

program
  .command('c <component>')
  .description('Create a Component')
  .action((componentName) => {
    // 創建一個 Component 工作區
    execSync(`ng g component ${componentName} --standalone `, { stdio: 'inherit' });

});

program
  .command('d <directive>')
  .description('Create a Directive')
  .action((directiveName) => {
    // 創建一個 Directive 工作區
    execSync(`ng g directive ${directiveName} --standalone `, { stdio: 'inherit' });

});

program
  .command('p <pipe>')
  .description('Create a Pipe')
  .action((pipeName) => {
    // 創建一個 Pipe 工作區
    execSync(`ng g pipe ${pipeName} --standalone `, { stdio: 'inherit' });

});

program.parse(process.argv);